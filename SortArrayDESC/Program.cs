﻿namespace SortArrayDESC;

public class Program
{
    static void Main(string[] args)
    {
        var sorted = SortArrayDesc(new int[] { 1 });
        Console.WriteLine(sorted);
    }

    public static int[] SortArrayDesc(int[] x)
    {
        //kết quả hàm sau khi thực thi là mảng số nguyên mới đã sắp xếp theo thứ tự giảm dần
        // Selection sort: O(n^2) -> maximum n = 
        var length = x.Length;
        // duyệt qua từng phần tử của mảng
        for (int i = 0; i < length - 1; i++)
        {
            // tìm phần tử lớn nhất trong mảng chưa sắp xếp
            var largestEle = i;
            for (int j = i + 1; j < length; j++)
            {
                if (x[j] > x[largestEle])
                {
                    largestEle = j;
                }
            }
            // hoán đổi vị trí của pt lớn nhất với pt đầu tiên của mảng chưa sắp xếp
            var tmp = x[largestEle];
            x[largestEle] = x[i];
            x[i] = tmp;
        }
        return x;
    }

}