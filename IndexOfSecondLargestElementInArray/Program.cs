﻿namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { 1, 2, 3 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        // kết quả hàm sau khi thực thi là vị trí(index) của số lớn thứ 2 trong mảng
        // Khi mảng chỉ có 1 phần tử trở xuống -> trả về -1
        if (x.Length <= 1) return -1;
        // Khi mảng có nhiều hơn 2 phần tử
        int maxValue, secondLargest, result, maxIndex;
        maxValue = secondLargest = int.MinValue;
        result = maxIndex = -1;
        // Tìm số lớn nhất
        for (int i = 0; i < x.Length; i++)
        {
            if (x[i] > maxValue)
            {
                maxValue = x[i];
                maxIndex = i;
            }
        }

        // Tìm số lớn thứ hai
        for (int i = 0; i < x.Length; i++)
        {
            if (x[i] > secondLargest && maxIndex != i)
            {
                secondLargest = x[i];
                result = i;
            }
        }
        return result;
    }
}